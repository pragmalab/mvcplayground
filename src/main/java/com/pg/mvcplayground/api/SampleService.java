package com.pg.mvcplayground.api;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

@Component
@Path("/sampleapi")
@Produces({MediaType.APPLICATION_JSON})
public class SampleService {

	@GET
	@Path("/test")
	public SampleService.SampleDTO test() {
		return new SampleDTO("Some name", new Date());
	}
	
	
	public static class SampleDTO {
		private String name;
		private Date date;
		
		
		public SampleDTO(String name, Date date) {
			super();
			this.name = name;
			this.date = date;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		
		
		
	}
	
}
